# Test-repo

This will be used to test pipelines

* This is Alpha test-run 
* This is Alpha test-run 1  # Doesn't trigger on commits
* This is Alpha test-run 2  # Doesn't trigger on merge-request merge [FAIL]
* This is Alpha test-run 4  # Tests our Alpha test-run 2    [PASS]
* This is Alpha test-run 5  # Trigger pipeline for Tag creation from the ui
* This is Alpha test-run 6
* This is Alpha test-run 7
* Beta test-run 1 - commit pipeline [PASS] 
* Beta test-run 2 - merge request pipeline [PASS] 
* Beta test-run 3 - tag pipeline (push) [PASS]